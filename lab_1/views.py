from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Stefan Mayer' # TODO Implement this
birth_date = 1999
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_date):
    return date.today().year - birth_date

